# README #
Basically the exact same tool as Rue once wrote, however;
his tool was outdated on many things such as no way to add
new signatures and so forth :(

So here is a new, modified version of his amazing tool with
a new twist; it now has the ability to load external databases
without much fuzz to it - it also includes a brand new byte-search
engine which allows true wildcard searches ;)

Included is a sample-database, from which you can see how you
can add new signatures ;)

Also, the design is written to resemble PEiD as much as possible of
one simple reason; simplicity of the mind, thus only the theme is
basically the difference between DNiD and PEiD ;)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Build ###
[![Build status](https://ci.appveyor.com/api/projects/status/s4rff1rmfh9295pv?svg=true)](https://ci.appveyor.com/project/styx2007/mu-dnid)